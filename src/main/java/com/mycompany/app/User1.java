package com.mycompany.app;

import org.junit.runner.JUnitCore;

import assignment.utility.Calc;

public class User1 {

	public static void main(String[] args){
		
		System.out.println("----------------Task 1-------------------");
		Employee[] employee = new Employee[5];
		
		employee[0] = new Employee(17244, "Kartik Gevariya");
		System.out.println(employee[0]);
		Employee.employeeCount++;
		 
		employee[1] = new Employee(17264, "Vatsal Rupavatiya");
		System.out.println(employee[1]);
		Employee.employeeCount++;
		
		employee[2] = new Employee(17265, "Vishal Patel");
		System.out.println(employee[2]);
		Employee.employeeCount++;
		
		System.out.println("Employee count : " + Employee.employeeCount);
		System.out.println();

		
		System.out.println("----------------Task 2-------------------");
		//To use packages from different drives use  'java -classpath c:\classes mypack.Calc'
		Calc calculator = new Calc();
		int value = 6;
		System.out.println("Square root of " + value + " is " + calculator.findRoot(value));
		
		/*JUnitCore.runClasses(CalcTest.class);*/
	}
}
