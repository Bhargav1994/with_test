package com.mycompany.app;

public class Employee {

	public static int employeeCount = 0;
	private int empID;
	private String empName;

	public Employee() {
		this.empID = empID;
		this.empName = empName;
	}
	public Employee(int empID, String empName) {
		this.empID = empID;
		this.empName = empName;
	}

	public static int getEmployeeCount() {
		return employeeCount;
	}

	public static void setEmployeeCount(int employeeCount) {
		Employee.employeeCount = employeeCount;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "EmplolyeeID : " + this.empID + "     " + "EmployeeName : " + this.empName;
	}
}
